/* global Twitch */
const options = {
  width: 854,
  height: 480,
  channel: 'waypoint'
}

const player = new Twitch.Player('video', options)
player.setVolume(0.5)
