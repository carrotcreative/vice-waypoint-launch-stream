# waypoint-launch-stream

static launch site for vice's launch of waypoint

## Setup

- make sure [node.js](http://nodejs.org) is at version >= `6`
- `npm i spike -g`
- clone this repo down and `cd` into the folder
- run `npm install`
- run `spike watch` or `spike compile`

## Deployment

- just push to master and it will deploy to netlify
- staging is at http://waypoint-livestream.netlify.com/ for now
