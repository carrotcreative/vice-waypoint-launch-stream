const htmlStandards = require('reshape-standard')
const cssStandards = require('spike-css-standards')
const jsStandards = require('babel-preset-latest')
const articles = require('./articles')

const baseUrl = 'https://waypoint-livestream.netlify.com/'

module.exports = {
  matchers: {
    html: '*(**/)*.sgr',
    css: '*(**/)*.sss'
  },
  ignore: ['**/layout.sgr', '**/_*', '**/.*', '_cache/**', 'readme.md', 'articles.json', 'yarn.lock'],
  reshape: (ctx) => {
    return htmlStandards({
      webpack: ctx,
      locals: { articles, baseUrl }
    })
  },
  postcss: (ctx) => {
    return cssStandards({
      webpack: ctx,
      features: {
        customProperties: {
          variables: { baseUrl }
        }
      }
    })
  },
  babel: { presets: [jsStandards] }
}
